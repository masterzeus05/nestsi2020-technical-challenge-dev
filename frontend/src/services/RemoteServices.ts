import Tasca from "../models/Tasca";
import Image from "@/models/Image";
import axios from "axios";
import Location from "@/models/Location";

const httpClient = axios.create();
httpClient.defaults.timeout = 10000;
httpClient.defaults.baseURL = process.env.VUE_APP_ROOT_API;
httpClient.defaults.headers.post["Content-Type"] = "application/json";
httpClient.defaults.headers["Access-Control-Allow-Origin"] = "*";
httpClient.interceptors.request.use(
  config => {
    return config;
  },
  error => Promise.reject(error)
);

const geoHttpClient = axios.create();
geoHttpClient.defaults.timeout = 10000;
geoHttpClient.defaults.baseURL = process.env.VUE_APP_GEO_API;
geoHttpClient.defaults.headers.post["Content-Type"] = "application/json";
geoHttpClient.defaults.headers["Access-Control-Allow-Origin"] = "*";
geoHttpClient.interceptors.request.use(
  config => {
    return config;
  },
  error => Promise.reject(error)
);

const addressSuffix = process.env.VUE_APP_DEFAULT_ADDRESS_SUFFIX;

export default class RemoteServices {
  static async getTascas(): Promise<Tasca[]> {
    return httpClient
      .get(`/tascas/`)
      .then(response => {
        return response.data.map((tasca: Tasca) => {
          return new Tasca(tasca);
        });
      })
      .catch(async error => {
        throw Error(await this.errorMessage(error));
      });
  }

  static async getTasca(id: number): Promise<Tasca> {
    return httpClient
      .get(`/tascas/${id}`)
      .then(response => {
        return new Tasca(response.data);
      })
      .catch(async error => {
        throw Error(await this.errorMessage(error));
      });
  }

  static async addTasca(tasca: Tasca): Promise<Tasca> {
    return httpClient
      .post(`/tascas/`, tasca)
      .then(response => {
        return new Tasca(response.data);
      })
      .catch(async error => {
        throw Error(await this.errorMessage(error));
      });
  }

  static async updateTasca(tasca: Tasca): Promise<Tasca> {
    return httpClient
      .put(`/tascas/${tasca.id}`, tasca)
      .then(response => {
        return new Tasca(response.data);
      })
      .catch(async error => {
        throw Error(await this.errorMessage(error));
      });
  }

  static async deleteTasca(tasca: Tasca) {
    return httpClient.delete(`/tascas/${tasca.id}`).catch(async error => {
      throw Error(await this.errorMessage(error));
    });
  }

  static async addImage(imageData: any): Promise<Image> {
    return httpClient
      .post(`/images/`, imageData)
      .then(response => {
        return new Image(response.data);
      })
      .catch(async error => {
        throw Error(await this.errorMessage(error));
      });
  }

  static async getImage(id: number): Promise<Image> {
    return httpClient
      .get(`/images/${id}`)
      .then(response => {
        return new Image(response.data);
      })
      .catch(async error => {
        throw Error(await this.errorMessage(error));
      });
  }

  static async getCoordinates(address: string): Promise<Location[]> {
    return geoHttpClient
      .get(
        `/search?q=${address}+${addressSuffix}&format=json&polygon_geojson=1&addressdetails=1`
      )
      .then(response => {
        return response.data.map((location: Location) => {
          return new Location(location);
        });
      })
      .catch(async error => {
        throw Error(await this.errorMessage(error));
      });
  }

  static async errorMessage(error: any): Promise<string> {
    if (error.message === "Network Error") {
      return "Unable to connect to server";
    } else if (error.message.split(" ")[0] === "timeout") {
      return "Request timeout - Server took too long to respond";
    } else if (error.response) {
      return error.response.data.message;
    } else {
      console.log(error);
      return "Unknown Error - Contact admin";
    }
  }
}
