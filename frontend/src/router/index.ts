import Vue from "vue";
import VueRouter, { RouteConfig } from "vue-router";
import About from "../views/About.vue";
import TascasListView from "../views/tascas/TascasListView.vue";
import TascaCreateView from "../views/tascas/TascaCreateView.vue";
import TascaDetailsView from "@/views/tascas/TascaDetailsView.vue";
import NotFound from "../views/NotFound.vue";

Vue.use(VueRouter);

const routes: Array<RouteConfig> = [
  {
    path: "/",
    name: "home",
    redirect: "/tascas/list",
    meta: {
      title: process.env.VUE_APP_NAME
    }
  },
  {
    path: "/about",
    name: "about",
    component: About,
    meta: {
      title: process.env.VUE_APP_NAME + " - About"
    }
  },
  {
    path: "/tascas/list",
    name: "tascas-list",
    component: TascasListView,
    meta: {
      title: process.env.VUE_APP_NAME + " - List Tascas"
    }
  },
  {
    path: "/tascas/create",
    name: "tascas-create",
    component: TascaCreateView,
    meta: {
      title: process.env.VUE_APP_NAME + " - Create Tasca"
    }
  },
  {
    path: "/tascas/:id(\\d+)",
    name: "tascas-details",
    component: TascaDetailsView,
    props: true,
    meta: {
      title: process.env.VUE_APP_NAME + " - Tasca Details"
    }
  },
  {
    path: "*",
    name: "not-found",
    component: NotFound,
    meta: {
      title: process.env.VUE_APP_NAME + " - Not Found"
    }
  }
];

const router = new VueRouter({
  routes,
  mode: "history"
});

router.afterEach(async (to, from) => {
  document.title = to.meta.title;
});

export default router;
