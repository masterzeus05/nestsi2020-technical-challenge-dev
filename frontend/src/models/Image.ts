export default class Image {
  id!: number;
  picByte!: string;

  constructor(jsonObj?: Image) {
    if (jsonObj) {
      this.id = jsonObj.id;
      this.picByte = jsonObj.picByte;
    }
  }
}
