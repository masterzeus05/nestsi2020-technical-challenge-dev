export default class Location {
  address!: string;
  display_name!: string;
  lat!: number;
  lon!: number;
  osm_id!: number;
  place_id!: number;
  type!: string;

  constructor(jsonObj?: Location) {
    if (jsonObj) {
      this.address = jsonObj.address;
      this.display_name = jsonObj.display_name;
      this.lat = Number(jsonObj.lat);
      this.lon = Number(jsonObj.lon);
      this.osm_id = jsonObj.osm_id;
      this.place_id = jsonObj.place_id;
      this.type = jsonObj.type;
    }
  }
}
