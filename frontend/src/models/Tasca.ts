import Image from "@/models/Image";

export default class Tasca {
  id!: number;
  name!: string;
  address!: string;
  rating!: number;
  imageId!: number;

  constructor(jsonObj?: Tasca) {
    if (jsonObj) {
      this.id = jsonObj.id;
      this.name = jsonObj.name;
      this.address = jsonObj.address;
      this.rating = jsonObj.rating;
      this.imageId = jsonObj.imageId;
    }
  }
}
