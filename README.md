
# NEST Summer Internship Challenge 2020
# Daniel Gonçalves

## Requirements

* Require download
  * [Postgres >= 10](https://www.postgresql.org/)
  * [Java 11](https://www.oracle.com/technetwork/java/javase/downloads/jdk11-downloads-5066655.html)
  * [Maven](https://maven.apache.org/download.cgi)
  * [Node 12.14](https://nodejs.org/en/)
* No download required
  * [Spring-boot](https://spring.io/)
  * [Vue.js](https://vuejs.org/)

## Installation

* Install all requirements that need download

* Start the database and create the database
```
sudo service postgresql start
sudo su -l postgres
createdb <database_name>
```
If on Windows, use `createdb -U <db_user> <database_name>`

* Rename `backend/src/main/resources/application-dev.properties.example` to `application-dev.properties` and fill its fields

* Run backend server
```
cd backend
mvn clean spring-boot:run
```

* Run frontend
```
cd frontend
npm i
npm start
```

* Access http://localhost:8080 


## Thank You

Thanks for the opportunity and the challenge!
