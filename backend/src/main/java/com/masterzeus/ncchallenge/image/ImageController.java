package com.masterzeus.ncchallenge.image;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("images")
public class ImageController {
    private final ImageRepository repository;

    public ImageController(ImageRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/")
    public List<Image> getAllImages() {
        return repository.findAll();
    }

    @PostMapping("/")
    public Image getNewImage(@RequestBody MultipartFile file) throws IOException {
        Image image = new Image();
        image.setPicByte(file.getBytes());
        return repository.save(image);
    }

    @GetMapping("/{id}")
    public Image getImage(@PathVariable Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new ImageNotFoundException(id));
    }

    @DeleteMapping("/{id}")
    public void deleteImage(@PathVariable Integer id) {
        repository.deleteById(id);
    }
}
