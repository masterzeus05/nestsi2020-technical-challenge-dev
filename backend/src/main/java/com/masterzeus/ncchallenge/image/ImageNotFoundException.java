package com.masterzeus.ncchallenge.image;

public class ImageNotFoundException extends RuntimeException {
    public ImageNotFoundException(Integer id) {
        super("Could not find image with id: " + id);
    }
}
