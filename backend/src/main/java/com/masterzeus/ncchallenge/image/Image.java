package com.masterzeus.ncchallenge.image;

import javax.persistence.*;

@Entity
public class Image {

    @Id
    @GeneratedValue
    private Integer id;

    private byte[] picByte;

    public Image() {
    }

    public Image(Integer id, byte[] picByte) {
        this.id = id;
        this.picByte = picByte;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getPicByte() {
        return picByte;
    }

    public void setPicByte(byte[] picByte) {
        this.picByte = picByte;
    }

    @Override
    public String toString() {
        return "Image{" +
                "id=" + id +
                '}';
    }
}
