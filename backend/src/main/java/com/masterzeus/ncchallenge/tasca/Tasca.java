package com.masterzeus.ncchallenge.tasca;

import com.masterzeus.ncchallenge.image.Image;

import javax.persistence.*;

@Entity
public class Tasca {

    @Id
    @GeneratedValue
    private Integer id;

    private String name;
    private String address;
    private Integer rating;

    private Integer imageId;

    public Tasca() {}

    public Tasca(String name, String address, Integer rating) {
        this.name = name;
        this.address = address;
        this.rating = rating;
    }

    public Integer getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return this.address;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String role) {
        this.address = role;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }

    @Override
    public String toString() {
        return "Tasca{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", rating=" + rating +
                '}';
    }
}
