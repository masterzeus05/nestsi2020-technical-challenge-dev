package com.masterzeus.ncchallenge.tasca;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class TascaNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(TascaNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String employeeNotFoundHandler(TascaNotFoundException ex) {
        return ex.getMessage();
    }
}
