package com.masterzeus.ncchallenge.tasca;

public class TascaNotFoundException extends RuntimeException {
    public TascaNotFoundException(Integer id) {
        super("Could not find tasca " + id);
    }
}
