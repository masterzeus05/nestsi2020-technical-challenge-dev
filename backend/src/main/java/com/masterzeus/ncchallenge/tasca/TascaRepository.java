package com.masterzeus.ncchallenge.tasca;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TascaRepository extends JpaRepository<Tasca, Integer> {
}
