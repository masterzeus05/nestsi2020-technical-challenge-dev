package com.masterzeus.ncchallenge.tasca;

import com.masterzeus.ncchallenge.image.ImageController;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("tascas")
class TascaController {
    private final TascaRepository repository;

    private final ImageController imageController;

    public TascaController(TascaRepository repository, ImageController imageController) {
        this.repository = repository;
        this.imageController = imageController;
    }

    @GetMapping("/")
    public List<Tasca> getAllTascas() {
        return repository.findAll();
    }

    @PostMapping("/")
    public Tasca createTasca(@RequestBody Tasca newTasca) {
        return repository.save(newTasca);
    }

    // Single item

    @GetMapping("/{id}")
    public Tasca getTasca(@PathVariable Integer id) {
        return repository.findById(id)
                .orElseThrow(() -> new TascaNotFoundException(id));
    }

    @PutMapping("/{id}")
    public Tasca replaceTasca(@RequestBody Tasca newTasca, @PathVariable Integer id) {
        Tasca tasca = repository.findById(id).orElseGet(() -> {
            newTasca.setId(id);
            return repository.save(newTasca);
        });

        Integer oldImageId = -1;
        if (tasca.getImageId() != null) oldImageId = tasca.getImageId();

        tasca.setName(newTasca.getName());
        tasca.setAddress(newTasca.getAddress());
        tasca.setRating(newTasca.getRating());
        if (newTasca.getImageId() != null) {
            tasca.setImageId(newTasca.getImageId());
        } else {
            oldImageId = -1;
        }
        repository.save(tasca);
        if (oldImageId != -1) imageController.deleteImage(oldImageId);
        return tasca;
    }

    @DeleteMapping("/{id}")
    public void deleteTasca(@PathVariable Integer id) {
        Tasca tasca = repository.findById(id).orElseThrow(() -> new TascaNotFoundException(id));

        repository.delete(tasca);
        if (tasca.getImageId() != null) imageController.deleteImage(tasca.getImageId());
    }
}
