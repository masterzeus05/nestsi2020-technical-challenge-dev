package com.masterzeus.ncchallenge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NcChallengeApplication {

	public static void main(String[] args) {
		SpringApplication.run(NcChallengeApplication.class, args);
	}

}
