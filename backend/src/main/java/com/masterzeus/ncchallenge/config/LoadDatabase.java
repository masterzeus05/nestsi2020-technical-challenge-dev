package com.masterzeus.ncchallenge.config;

import com.masterzeus.ncchallenge.image.Image;
import com.masterzeus.ncchallenge.image.ImageRepository;
import com.masterzeus.ncchallenge.tasca.Tasca;
import com.masterzeus.ncchallenge.tasca.TascaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;
import java.io.InputStream;

@Configuration
class LoadDatabase {
    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Value("${data.generate}")
    private String generate;

    @Bean
    CommandLineRunner initDatabase(TascaRepository repository, ImageRepository imageRepository) throws IOException {
        if (repository.count() == 0) {
            // Add image from Mijacao
            Image testImage = new Image();
            InputStream inputStream = getClass().getClassLoader().getResourceAsStream("img/mijacao.jpg");
            testImage.setPicByte(inputStream.readAllBytes());
            imageRepository.save(testImage);
            // Add tasca mijacao
            Tasca mijacao = new Tasca("Mija Cão", "Rua Nova 8, 3000-297 Coimbra", 7);
            mijacao.setImageId(testImage.getId());

            // Add image from Ze Manel dos Ossos
            Image testImage2 = new Image();
            InputStream inputStream2 = getClass().getClassLoader().getResourceAsStream("img/ZeManelDosOssos.jpg");
            testImage2.setPicByte(inputStream2.readAllBytes());
            imageRepository.save(testImage2);
            // Add tasca Ze Manel dos Ossos
            Tasca zemaneldosossos = new Tasca("Zé Manel dos Ossos", "Beco do Forno 10 122, 3000-192 Coimbra", 9);
            zemaneldosossos.setImageId(testImage2.getId());

            repository.save(mijacao);
            repository.save(zemaneldosossos);

            return args -> {
                log.info("Preloading " + mijacao);
                log.info("Preloading " + zemaneldosossos);
                log.info("Preloading " + repository.save(new Tasca("Tasca do Aires", "Rua das Rãs, número 23", 8)));
                log.info("Preloading " + repository.save(new Tasca("Tasca do bandido", "Avenida da escapada, nº75", 5)));
                log.info("Preloading " + repository.save(new Tasca("Café \"o Victor\"", "Rua das Padeiras, 44", 8)));
                log.info("Preloading " + repository.save(new Tasca("Tasc'om ela", "Rua dos Alvíceras, 54", 10)));

            };
        }

        return args -> {};
    }
}
